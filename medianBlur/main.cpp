#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace std;
using namespace cv;



int main(){

    Mat image = imread("../pic/1.png");
	if (image.empty()) {
        cout << "image.empty" << endl;
		return -1;
	}

    Mat image2;
    // 第三个参数，int类型的ksize，孔径的线性尺寸（aperture linear size），注意这个参数必须是大于1的奇数，比如：3，5，7，9 ...
    medianBlur(image, image2, 11);

    imwrite("../pic/result.png", image2);
    return 0;
}