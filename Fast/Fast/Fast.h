#ifndef HSVDIFF_H
#define HSVDIFF_H
#include <iostream>
#include <algorithm>
#include <vector>


struct OnePoint2f
{
    float x;
    float y;
};
typedef struct __KeyPoint_
{
    OnePoint2f pt; 
    float size; 
    float angle;

    float response; 
    int octave;

}OneKeyPoint;

enum MyEnum
{
    start = -1,
    nOAST_9_16,
    nAGAST_5_8,

};

// @para img 灰度图
// @para mWidth 图像宽
// @para mHeight 图像高
// @para keypoints 结果
// @para threshold 阈值
void OAST_9_16_(unsigned char * img, int mWidth, int mHeight, std::vector<OneKeyPoint>& keypoints, int threshold);


class Fast{

public:
  Fast();
  ~Fast();
  
private:

};



#endif
