#include <iostream>
#include <thread>

#include <opencv2/opencv.hpp>

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " <use_threads: 1|0>" << std::endl;
        return 0;
    }

    while(1){
        const bool use_threads = std::stoi(argv[1]);

        cv::Mat img(512, 512, CV_8UC3);
        cv::randu(img, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));

        auto callable = [&img]() {
            cv::Mat img2;
            cv::resize(img, img2, cv::Size(), 0.5, 0.5);
        };

        if (use_threads) {
            for (size_t i = 0; i < 10000; ++i) {
                std::thread { callable }.join();
            }
        } else {
            for (size_t i = 0; i < 10000; ++i) {
                callable();
            }
        }
    }


    return 0;
}


