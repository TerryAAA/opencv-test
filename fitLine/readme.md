## opencv直线拟合
void fitLine( InputArray points, OutputArray line, int distType,
                           double param, double reps, double aeps );

- points：输入待拟合直线的2D或者3D点集。

- line：输出描述直线的参数，2D点集描述参数为Vec4f类型，3D点集描述参数为Vec6f类型。

- distType：M-estimator算法使用的距离类型标志，可以选择的距离类型在表7-1中给出。一般常选择cv.DIST_L2
    cv2.DIST_USER : User defined distance
    cv2.DIST_L1: distance = |x1-x2| + |y1-y2|
    cv2.DIST_L2: 欧式距离，此时与最小二乘法相同
    cv2.DIST_C:distance = max(|x1-x2|,|y1-y2|)
    cv2.DIST_L12:L1-L2 metric: distance = 2(sqrt(1+x*x/2) - 1))
    cv2.DIST_FAIR:distance = c^2(|x|/c-log(1+|x|/c)), c = 1.3998
    cv2.DIST_WELSCH: distance = c2/2(1-exp(-(x/c)2)), c = 2.9846
    cv2.DIST_HUBER:distance = |x|<c ? x^2/2 : c(|x|-c/2), c=1.345

- param：某些类型距离的数值参数（C）。如果数值为0，则自动选择最佳值。一般常选择0。

- reps：坐标原点与直线之间的距离精度，数值0表示选择自适应参数，一般常选择0.01。

- aeps：直线角度精度，数值0表示选择自适应参数，一般常选择0.01。




cv.line、cv.circle 画图： https://blog.csdn.net/m0_38082783/article/details/127534273 