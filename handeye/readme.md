## opencv手眼标定

void cv::calibrateHandEye(InputArrayOfArrays    R_gripper2base,
                          InputArrayOfArrays    t_gripper2base,
                          InputArrayOfArrays    R_target2cam,
                          InputArrayOfArrays    t_target2cam,
                          OutputArray           R_cam2gripper,
                          OutputArray           t_cam2gripper,
                          HandEyeCalibrationMethod method = CALIB_HAND_EYE_TSAI)	

R_gripper2base，t_gripper2base是机械臂抓手相对于机器人基坐标系的旋转矩阵与平移向量；
R_target2cam ， t_target2cam 是标定板相对于相机的位姿矩阵到（calibrateCamera()得到）； R_cam2gripper ， t_cam2gripper是求解的手眼矩阵分解得到的旋转矩阵与平移矩阵；
OpenCV实现了5种方法求取手眼矩阵 ， Tsai两步法速度最快。






