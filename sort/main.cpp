#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace std;
using namespace cv;


/*

Mat类构造与赋值 :
https://mp.weixin.qq.com/s?__biz=MzU0NjgzMDIxMQ==&mid=2247486299&idx=1&sn=14af1a1a7ae8ef752bd2e94d7b8f6dc5&chksm=fb56e9b7cc2160a1324ce04836457a4ce4ebf7d04d45085feb9e5e2681cdee8b0ff3981ef4f2&scene=27


void sort(InputArray src, OutputArray dst, int flags)

flags:
    SORT_EVERY_ROW       // 每个矩阵行独立排序
    SORT_EVERY_COLUMN    // 每个矩阵列都独立排序
    SORT_ASCENDING       // 每个矩阵行按升序排序
    SORT_DESCENDING      // 每个矩阵行按降序排序
*/

void claBitmaps(Mat &img, Mat &tb, Mat &eb){
    Mat tmp = img.reshape(1, 1);//make matrix new number of channels and new number of rows. here Put data: 1 row, all cols 
    Mat sorted; //after sorted data
    cv::sort(tmp, sorted, SORT_EVERY_ROW);
    int meddate = sorted.at<uchar>(sorted.cols / 2);//find median data in median of cols
    cout << "meddate"<<meddate << endl;
    threshold(img, tb, meddate, 255, THRESH_BINARY);
    imshow("tb", tb);
    waitKey(0);
}

int main(){
    float mul_num[9][1] = { 9.1, 8.1, 7.1, 6.1, 5.1, 4.1, 3.1, 2.1, 1.1 }; // 或者float mul_num[9][1] = { ...也可

    cv::Mat m1(1,9, CV_32FC1,mul_num);// cv::Mat M = cv::Mat(height,width,,data)

    // cv::Mat m1(cv::Size(9,1), CV_32FC1); // 32位浮点型单通道，1行9列 ，cv::Size(宽，高)
    // for(int i = 0; i < m1.rows; i++){ //矩阵行数循环
    //     for (int j = 0; j < m1.cols; j++){ //矩阵列数循环
    //         m1.at<float>(i, j) = mul_num[j][i];
    //     }
    // }

    // cv::Mat m1 = (cv::Mat_<float>(1, 9) << 9.1, 8.1, 7.1, 6.1, 5.1, 4.1, 3.1, 2.1, 1.1); // cv::Mat_(height,width) << ...

    std::cout << "m1.rows: " << m1.rows << "    m1.cols: " << m1.cols << std::endl;
    std::cout << "m1: \n" << m1 << std::endl;

    Mat sorted; //after sorted data
    cv::sort(m1, sorted, SORT_EVERY_ROW);
    std::cout << "sorted.rows: " << sorted.rows << "    sorted.cols: " << sorted.cols << std::endl;
    std::cout << "sorted: \n" << sorted << std::endl;

    float middle_date = sorted.at<float>(sorted.cols / 2);//find median data in median of cols
    cout << "middle_date: "<<middle_date << endl;


    return 0;
}