#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include "hsvdiff.h"



using namespace std;
using namespace cv;



Diff::Diff(void){

  Mat img, imgHSV, imgH, imgS, imgV;
  vector<Mat> imgHSVs;
  int nRows, nCols, i, j;
  string imgPath  = "../pic/1.jpg";
  string imgHPath = "../pic/imgH.jpg";
  string imgSPath = "../pic/imgS.jpg";
  string imgVPath = "../pic/imgV.jpg";
  string diffHPath = "../pic/diffH.jpg";
  string diffSPath = "../pic/diffS.jpg";
  string diffVPath = "../pic/diffV.jpg";
  cout << "load parameter succeed." << endl;
  
  /* 读取图片 */
  img = imread(imgPath);
  nRows = img.rows;
	nCols = img.cols;
  cout << "read image ok." << "  width: " << nRows << "  heigh: " << nCols << endl;
  
  /* 颜色空间转换，提取，保存 */
  cvtColor(img, imgHSV, COLOR_BGR2HSV);
  split(imgHSV , imgHSVs);
  imgH = imgHSVs[0];
  imgS = imgHSVs[1];
  imgV = imgHSVs[2];
  imwrite(imgHPath, imgH);
  imwrite(imgSPath, imgS);
  imwrite(imgVPath, imgV);
  cout << "save imgH/S/V ok." << endl;
  
 
  /* H通道差分 */
  for (i = 0; i < nRows-1; i++){
    for (j = 0; j < nCols-1; j++){
      imgH.at<uchar>(i, j) = abs(imgH.at<uchar>(i + 1, j ) - imgH.at<uchar>(i, j)) + abs(imgH.at<uchar>(i , j + 1) - imgH.at<uchar>(i, j));
    }
  }
  
  /* S通道差分 */
  for (i = 0; i < nRows-1; i++){
    for (j = 0; j < nCols-1; j++){
      imgS.at<uchar>(i, j) = abs(imgS.at<uchar>(i + 1, j ) - imgS.at<uchar>(i, j)) + abs(imgS.at<uchar>(i , j + 1) - imgS.at<uchar>(i, j));
    }
  }
  
  /* V通道差分 */
  for (i = 0; i < nRows-1; i++){
    for (j = 0; j < nCols-1; j++){
      imgV.at<uchar>(i, j) = abs(imgV.at<uchar>(i + 1, j ) - imgV.at<uchar>(i, j)) + abs(imgV.at<uchar>(i , j + 1) - imgV.at<uchar>(i, j));
    }
  }
  
  /* 保存差分图片 */
  imwrite(diffHPath, imgH);
  imwrite(diffSPath, imgS);
  imwrite(diffVPath, imgV);
  cout << "save diffH/S/V ok." << endl;
}


Diff::~Diff(void){
  cout << "byby ~~~ I'm jianbao" << endl;
}





