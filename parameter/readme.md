 ## opencv参数读取yaml文件
参考文章： https://blog.csdn.net/weixin_43892514/article/details/105836267
FileStorage 以 FileNode 为单位存储数据，且无法删改某个已有 FileNode的内容

1.
参数文件一定要有
%YAML:1.0
---
否则会报错

如：
%YAML:1.0
---
File.version: "1.0"

a_int_value: 10
b_float_value: 11.
c_string_value: "Valar Morghulis Valar Dohaeris"



2.
可以实例化的时候直接指定文件和操作方式，如： FileStorage fs(configPath, FileStorage::READ);
也可以调用方法，如： FileStorage fs; fs.open(path_file_to_read, FileStorage::READ)

FileStorage::WRITE ，覆盖写
FileStorage::APPEND ，追加写
FileStorage::READ ，读取

fs.open(configPath, FileStorage::WRITE);会在文件最开头书写
%YAML:1.0
---

每运行一次fs.open(configPath, FileStorage::APPEND);会在文件里打印：
...
---


3.
初始化后记得用 fs.isOpened() 判断一下



4.
基于流的处理, 使用 << 进行 WRITE, 使用 >> 进行 READ.
如读取：fs["a_int_value"] >> a; 或者 a = (int)fs["a_int_value"];
如写入：fs << "a_int_value" << 10; 或者 a = 10; fs << "a_int_value" << a;
注意写的时候不要加 / 这样子的符号，修改会在FileStorage析构的时候（fs.release();）生效
（-5：错误参数）在函数“writeScalar”中，键名称只能包含字母数字字符[a-zA-Z0-9]、“-”、“_”和“”

5.
支持普通的 int/float/double/char/string 等类型，支持opencv矩阵 cv::Mat 格式，支持 vector 格式，支持 map 格式。
各种数据类型支持嵌套
对于 vector 结构的输出和输出，要注意在第一个元素前加上 [ ，在最后一个元素后加上 ] 
如：
    fs << "e_vector" << "[";
    fs << "One" << "Two" << "Three";
    fs << "]";
    或者
    fs << "e_vector" <<"[" << "One" << "Two" << "Three" << "]"; 
对于 map 结构的输出和输出，要注意在第一个元素前加上 { ，在最后一个元素后加上 } 
如：
    fs << "f_map" << "{";
    fs << "One" << 1;
    fs << "Two" << 2 ;
    fs << "}";
    或者
    fs << "f_map" << "{" << "One" << 1 << "Two" << 2 << "}";

vector 与 map 结构可以相互嵌套，并且读取也需要使用 FileNode ，如： FileNode n = fs["e_vector_value"]; n >> e;
对于一连串的node，可以使用 FileNodeIterator 结构，具体见代码



6.
FileNode 类型校验
if (n.type() != FileNode::SEQ)  
{
    cerr << "e_vector_value is not a sequence! FAIL" << endl;  
    return 1;  
}
enum  	{
  NONE = 0,
  INT = 1,
  REAL = 2,
  FLOAT = REAL,
  STR = 3,
  STRING = STR,
  SEQ = 4,
  MAP = 5,
  TYPE_MASK = 7,
  FLOW = 8,
  UNIFORM = 8,
  EMPTY = 16,
  NAMED = 32
}
可以通过cout << n.type() << endl;先看看属于什么类型



7.
最后记得 fs.release();






