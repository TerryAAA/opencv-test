#include <opencv2/imgcodecs.hpp>
#include <opencv2/stitching.hpp>
#include <iostream>
#include "stitcher.h"


// 图片拼接代码，参考如下链接
// https://docs.opencv.org/4.5.1/d1/d46/group__stitching.html
// https://docs.opencv.org/master/d8/d19/tutorial_stitcher.html
using namespace std;
using namespace cv;



Stitching::Stitching(void){

  Mat img;
  Mat imgS;
	Stitcher::Mode mode;
	vector<Mat> imgs;


  int nRows, nCols, i, j;
  string  result_name = "../pic/result.jpg";	// 输出结果存放
  string img1 = "../pic/1.jpg";
  string img2 = "../pic/2.jpg";
  mode = Stitcher::PANORAMA;		// 或者mode = Stitcher::SCANS;前者是普通模式，后者是专业模式
  cout << "load parameter succeed." << endl;
  

  /* 读取图片 */
  img = imread(img1);
  nRows = img.rows;
	nCols = img.cols;
  cout << "read img1 ok." << "  width1: " << nRows << "  heigh1: " << nCols << endl;
  imgs.push_back(img);		//将读到的图片依次放到imgs里，push_back为vector标准类模板操作函数，指在尾端加入数据
  img = imread(img2);
  nRows = img.rows;
	nCols = img.cols;
  cout << "read img2 ok." << "  width2: " << nRows << "  heigh2: " << nCols << endl;
  imgs.push_back(img);		//将读到的图片依次放到imgs里，push_back为vector标准类模板操作函数，指在尾端加入数据
  
  /* 图像拼接 */
  //![stitching]
  Mat pano;
  Ptr<Stitcher> stitcher = Stitcher::create(mode);  		// 创建了一个新的stitcher实例，Ptr<>是一个类模板，这里表示继承Stitcher
  Stitcher::Status status = stitcher->stitch(imgs, pano); 	// 将imgs合并到pano。并返回一个状态。

  if (status != Stitcher::OK)
  {
    cout << "Can't stitch images, error code = " << int(status) << endl;
    //return EXIT_FAILURE;
  }
  //![stitching]

  	
  cout << "stitching completed successfully\n" << result_name << " saved!";
  
 

  /* 保存拼接图片 */
  imwrite(result_name, pano);                			// 保存图片   	
  cout << "save stitching ok." << endl;
}


Stitching::~Stitching(void){
  cout << "byby ~~~ I'm jianbao" << endl;
}





